## Telegram messenger for Android

Telegram是一个跨平台的即时通信软件，它的客户端是自由及开放源代码软件，但是它的服务器是专有软件。用户可以相互交换加密与自析构的消息，发送照片、影片等所有类型文件。官方提供手机版（Android、iOS、Windows Phone）、桌面版（Windows、macOS、Linux）和网页版等多种平台客户端；同时官方开放应用程序接口。

官方开源代码有很多问题，这个分支基于TelegramV4.6分支编译，下载完毕之后直接配置NDK运行即可，API key需要自行替换更新
